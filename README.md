## Secret Manager

Terraform configuration for Secret manager. This configuration handles

- Secrets used by Cloud Run deployments

### Note

Secrets can be created from the configuration. However, secret **versions** must not be created from the configuration.
Use the console to create **versions**.