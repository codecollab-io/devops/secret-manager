//                                                        $$\
//                                                        $$ |
// $$$$$$\   $$$$$$\   $$$$$$\  $$\  $$$$$$\   $$$$$$$\ $$$$$$\    $$$$$$$\
//$$  __$$\ $$  __$$\ $$  __$$\ \__|$$  __$$\ $$  _____|\_$$  _|  $$  _____|
//$$ /  $$ |$$ |  \__|$$ /  $$ |$$\ $$$$$$$$ |$$ /        $$ |    \$$$$$$\
//$$ |  $$ |$$ |      $$ |  $$ |$$ |$$   ____|$$ |        $$ |$$\  \____$$\
//$$$$$$$  |$$ |      \$$$$$$  |$$ |\$$$$$$$\ \$$$$$$$\   \$$$$  |$$$$$$$  |
//$$  ____/ \__|       \______/ $$ | \_______| \_______|   \____/ \_______/
//$$ |                    $$\   $$ |
//$$ |                    \$$$$$$  |
//\__|                     \______/

output "cc_projects_service_qa_config_secret_id" {
  value = google_secret_manager_secret.cc_projects_service_qa_config.secret_id
}

output "cc_projects_service_config_secret_id" {
  value = google_secret_manager_secret.cc_projects_service_config.secret_id
}

//                          $$\
//                          $$ |
//      $$\ $$\  $$\  $$\ $$$$$$\
//      \__|$$ | $$ | $$ |\_$$  _|
//      $$\ $$ | $$ | $$ |  $$ |
//      $$ |$$ | $$ | $$ |  $$ |$$\
//      $$ |\$$$$$\$$$$  |  \$$$$  |
//      $$ | \_____\____/    \____/
//$$\   $$ |
//\$$$$$$  |
// \______/

output "jwt_public_key_qa_secret_id" {
  value = google_secret_manager_secret.jwt_public_key_qa.secret_id
}

output "jwt_public_key_secret_id" {
  value = google_secret_manager_secret.jwt_public_key.secret_id
}

output "jwt_private_key_qa_secret_id" {
  value = google_secret_manager_secret.jwt_private_key_qa.secret_id
}

output "jwt_private_key_secret_id" {
  value = google_secret_manager_secret.jwt_private_key.secret_id
}


//                      $$\     $$\
//                      $$ |    $$ |
// $$$$$$\  $$\   $$\ $$$$$$\   $$$$$$$\
// \____$$\ $$ |  $$ |\_$$  _|  $$  __$$\
// $$$$$$$ |$$ |  $$ |  $$ |    $$ |  $$ |
//$$  __$$ |$$ |  $$ |  $$ |$$\ $$ |  $$ |
//\$$$$$$$ |\$$$$$$  |  \$$$$  |$$ |  $$ |
// \_______| \______/    \____/ \__|  \__|

output "cc_auth_qa_config_secret_id" {
  value = google_secret_manager_secret.cc_auth_qa.secret_id
}

output "cc_auth_config_secret_id" {
  value = google_secret_manager_secret.cc_auth.secret_id
}

//                    $$\                              $$\
//                    \__|                             $$ |
// $$$$$$\   $$$$$$\  $$\          $$$$$$\   $$$$$$\ $$$$$$\    $$$$$$\  $$\  $$\  $$\  $$$$$$\  $$\   $$\
// \____$$\ $$  __$$\ $$ |$$$$$$\ $$  __$$\  \____$$\\_$$  _|  $$  __$$\ $$ | $$ | $$ | \____$$\ $$ |  $$ |
// $$$$$$$ |$$ /  $$ |$$ |\______|$$ /  $$ | $$$$$$$ | $$ |    $$$$$$$$ |$$ | $$ | $$ | $$$$$$$ |$$ |  $$ |
//$$  __$$ |$$ |  $$ |$$ |        $$ |  $$ |$$  __$$ | $$ |$$\ $$   ____|$$ | $$ | $$ |$$  __$$ |$$ |  $$ |
//\$$$$$$$ |$$$$$$$  |$$ |        \$$$$$$$ |\$$$$$$$ | \$$$$  |\$$$$$$$\ \$$$$$\$$$$  |\$$$$$$$ |\$$$$$$$ |
// \_______|$$  ____/ \__|         \____$$ | \_______|  \____/  \_______| \_____\____/  \_______| \____$$ |
//          $$ |                  $$\   $$ |                                                     $$\   $$ |
//          $$ |                  \$$$$$$  |                                                     \$$$$$$  |
//          \__|                   \______/                                                       \______/

output "cc_api_gateway_qa_config_secret_id" {
  value = google_secret_manager_secret.cc_api_gateway_qa.secret_id
}

output "cc_api_gateway_us_config_secret_id" {
  value = google_secret_manager_secret.cc_api_gateway_us.secret_id
}

output "cc_api_gateway_sg_config_secret_id" {
  value = google_secret_manager_secret.cc_api_gateway_sg.secret_id
}

output "cc_api_gateway_eu_config_secret_id" {
  value = google_secret_manager_secret.cc_api_gateway_eu.secret_id
}
//                                            $$\ $$\                                                                                       $$\
//                                            \__|$$ |                                                                                      $$ |
// $$$$$$$\  $$$$$$\  $$$$$$\$$$$\   $$$$$$\  $$\ $$ | $$$$$$\   $$$$$$\         $$$$$$$\  $$$$$$\   $$$$$$\ $$\    $$\ $$$$$$\  $$$$$$$\ $$$$$$\
//$$  _____|$$  __$$\ $$  _$$  _$$\ $$  __$$\ $$ |$$ |$$  __$$\ $$  __$$\       $$  _____|$$  __$$\ $$  __$$\\$$\  $$  |\____$$\ $$  __$$\\_$$  _|
//$$ /      $$ /  $$ |$$ / $$ / $$ |$$ /  $$ |$$ |$$ |$$$$$$$$ |$$ |  \__|      \$$$$$$\  $$$$$$$$ |$$ |  \__|\$$\$$  / $$$$$$$ |$$ |  $$ | $$ |
//$$ |      $$ |  $$ |$$ | $$ | $$ |$$ |  $$ |$$ |$$ |$$   ____|$$ |             \____$$\ $$   ____|$$ |       \$$$  / $$  __$$ |$$ |  $$ | $$ |$$\
//\$$$$$$$\ \$$$$$$  |$$ | $$ | $$ |$$$$$$$  |$$ |$$ |\$$$$$$$\ $$ |            $$$$$$$  |\$$$$$$$\ $$ |        \$  /  \$$$$$$$ |$$ |  $$ | \$$$$  |
// \_______| \______/ \__| \__| \__|$$  ____/ \__|\__| \_______|\__|            \_______/  \_______|\__|         \_/    \_______|\__|  \__|  \____/
//                                  $$ |
//                                  $$ |
//                                  \__|

output "cc_compiler_servant_qa_config_secret_id" {
  value = google_secret_manager_secret.cc_compiler_servant_qa.secret_id
}

output "cc_compiler_servant_us_config_secret_id" {
  value = google_secret_manager_secret.cc_compiler_servant_us.secret_id
}

output "cc_compiler_servant_eu_config_secret_id" {
  value = google_secret_manager_secret.cc_compiler_servant_eu.secret_id
}

output "cc_compiler_servant_sg_config_secret_id" {
  value = google_secret_manager_secret.cc_compiler_servant_sg.secret_id
}

//                                            $$\ $$\                                                                 $$\
//                                            \__|$$ |                                                                $$ |
// $$$$$$$\  $$$$$$\  $$$$$$\$$$$\   $$$$$$\  $$\ $$ | $$$$$$\   $$$$$$\          $$$$$$\$$$$\   $$$$$$\   $$$$$$$\ $$$$$$\    $$$$$$\   $$$$$$\
//$$  _____|$$  __$$\ $$  _$$  _$$\ $$  __$$\ $$ |$$ |$$  __$$\ $$  __$$\ $$$$$$\ $$  _$$  _$$\  \____$$\ $$  _____|\_$$  _|  $$  __$$\ $$  __$$\
//$$ /      $$ /  $$ |$$ / $$ / $$ |$$ /  $$ |$$ |$$ |$$$$$$$$ |$$ |  \__|\______|$$ / $$ / $$ | $$$$$$$ |\$$$$$$\    $$ |    $$$$$$$$ |$$ |  \__|
//$$ |      $$ |  $$ |$$ | $$ | $$ |$$ |  $$ |$$ |$$ |$$   ____|$$ |              $$ | $$ | $$ |$$  __$$ | \____$$\   $$ |$$\ $$   ____|$$ |
//\$$$$$$$\ \$$$$$$  |$$ | $$ | $$ |$$$$$$$  |$$ |$$ |\$$$$$$$\ $$ |              $$ | $$ | $$ |\$$$$$$$ |$$$$$$$  |  \$$$$  |\$$$$$$$\ $$ |
// \_______| \______/ \__| \__| \__|$$  ____/ \__|\__| \_______|\__|              \__| \__| \__| \_______|\_______/    \____/  \_______|\__|
//                                  $$ |
//                                  $$ |
//                                  \__|

output "cc_compiler_master_qa_config_secret_id" {
  value = google_secret_manager_secret.cc_compiler_master_qa.secret_id
}

output "cc_compiler_master_us_config_secret_id" {
  value = google_secret_manager_secret.cc_compiler_master_us.secret_id
}

output "cc_compiler_master_eu_config_secret_id" {
  value = google_secret_manager_secret.cc_compiler_master_eu.secret_id
}

output "cc_compiler_master_sg_config_secret_id" {
  value = google_secret_manager_secret.cc_compiler_master_sg.secret_id
}

//                $$\ $$\   $$\                                                   $$\
//                $$ |\__|  $$ |                                                  \__|
// $$$$$$\   $$$$$$$ |$$\ $$$$$$\    $$$$$$\   $$$$$$\         $$$$$$\   $$$$$$\  $$\
//$$  __$$\ $$  __$$ |$$ |\_$$  _|  $$  __$$\ $$  __$$\        \____$$\ $$  __$$\ $$ |
//$$$$$$$$ |$$ /  $$ |$$ |  $$ |    $$ /  $$ |$$ |  \__|       $$$$$$$ |$$ /  $$ |$$ |
//$$   ____|$$ |  $$ |$$ |  $$ |$$\ $$ |  $$ |$$ |            $$  __$$ |$$ |  $$ |$$ |
//\$$$$$$$\ \$$$$$$$ |$$ |  \$$$$  |\$$$$$$  |$$ |            \$$$$$$$ |$$$$$$$  |$$ |
// \_______| \_______|\__|   \____/  \______/ \__|             \_______|$$  ____/ \__|
//                                                                      $$ |
//                                                                      $$ |
//                                                                      \__|

output "cc_editor_api_qa_config_secret_id" {
  value = google_secret_manager_secret.cc_editor_api_qa.secret_id
}

output "cc_editor_api_us_config_secret_id" {
  value = google_secret_manager_secret.cc_editor_api_us.secret_id
}

output "cc_editor_api_sg_config_secret_id" {
  value = google_secret_manager_secret.cc_editor_api_sg.secret_id
}

output "cc_editor_api_eu_config_secret_id" {
  value = google_secret_manager_secret.cc_editor_api_eu.secret_id
}
//                    $$\
//                    \__|
// $$$$$$\   $$$$$$\  $$\
// \____$$\ $$  __$$\ $$ |
// $$$$$$$ |$$ /  $$ |$$ |
//$$  __$$ |$$ |  $$ |$$ |
//\$$$$$$$ |$$$$$$$  |$$ |
// \_______|$$  ____/ \__|
//          $$ |
//          $$ |
//          \__|

output "cc_api_qa_config_secret_id" {
  value = google_secret_manager_secret.cc_api_qa.secret_id
}

output "cc_api_config_secret_id" {
  value = google_secret_manager_secret.cc_api.secret_id
}

//          $$\
//          $$ |
// $$$$$$$\ $$$$$$$\   $$$$$$\   $$$$$$\   $$$$$$\
//$$  _____|$$  __$$\  \____$$\ $$  __$$\ $$  __$$\
//\$$$$$$\  $$ |  $$ | $$$$$$$ |$$ |  \__|$$$$$$$$ |
// \____$$\ $$ |  $$ |$$  __$$ |$$ |      $$   ____|
//$$$$$$$  |$$ |  $$ |\$$$$$$$ |$$ |      \$$$$$$$\
//\_______/ \__|  \__| \_______|\__|       \_______|

output "cc_share_qa_config_secret_id" {
  value = google_secret_manager_secret.cc_share_qa.secret_id
}

output "cc_share_us_config_secret_id" {
  value = google_secret_manager_secret.cc_share_us.secret_id
}

output "cc_share_eu_config_secret_id" {
  value = google_secret_manager_secret.cc_share_eu.secret_id
}

output "cc_share_sg_config_secret_id" {
  value = google_secret_manager_secret.cc_share_sg.secret_id
}

//$$\        $$\                   $$\
//$$ |       $$ |                  $$ |
//$$$$$$$\ $$$$$$\   $$$$$$\$$$$\  $$ |         $$$$$$$\  $$$$$$\   $$$$$$\ $$\    $$\  $$$$$$\   $$$$$$\
//$$  __$$\\_$$  _|  $$  _$$  _$$\ $$ |$$$$$$\ $$  _____|$$  __$$\ $$  __$$\\$$\  $$  |$$  __$$\ $$  __$$\
//$$ |  $$ | $$ |    $$ / $$ / $$ |$$ |\______|\$$$$$$\  $$$$$$$$ |$$ |  \__|\$$\$$  / $$$$$$$$ |$$ |  \__|
//$$ |  $$ | $$ |$$\ $$ | $$ | $$ |$$ |         \____$$\ $$   ____|$$ |       \$$$  /  $$   ____|$$ |
//$$ |  $$ | \$$$$  |$$ | $$ | $$ |$$ |        $$$$$$$  |\$$$$$$$\ $$ |        \$  /   \$$$$$$$\ $$ |
//\__|  \__|  \____/ \__| \__| \__|\__|        \_______/  \_______|\__|         \_/     \_______|\__|

output "cc_html_server_qa_config_secret_id" {
  value = google_secret_manager_secret.cc_html_server_qa.secret_id
}

output "cc_html_server_us_config_secret_id" {
  value = google_secret_manager_secret.cc_html_server_us.secret_id
}

output "cc_html_server_eu_config_secret_id" {
  value = google_secret_manager_secret.cc_html_server_eu.secret_id
}

output "cc_html_server_sg_config_secret_id" {
  value = google_secret_manager_secret.cc_html_server_sg.secret_id
}
//$$\   $$\     $$\
//$$ |  $$ |    \__|
//$$ |$$$$$$\   $$\
//$$ |\_$$  _|  $$ |
//$$ |  $$ |    $$ |
//$$ |  $$ |$$\ $$ |
//$$ |  \$$$$  |$$ |
//\__|   \____/ \__|

output "cc_lti_qa_config_secret_id" {
  value = google_secret_manager_secret.cc_lti_qa.secret_id
}

output "cc_lti_config_secret_id" {
  value = google_secret_manager_secret.cc_lti.secret_id
}