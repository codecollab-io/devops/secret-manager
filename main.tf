terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.81.0"
    }
  }
}

variable "GCP_SERVICE_ACCOUNT" {
  type = string
}

variable "GCP_SERVICE_KEY" {
  type = string
}

provider "google" {
  project     = "codecollab-io"
  region      = "us-central1"
  credentials = var.GCP_SERVICE_KEY
}

//                                                        $$\
//                                                        $$ |
// $$$$$$\   $$$$$$\   $$$$$$\  $$\  $$$$$$\   $$$$$$$\ $$$$$$\    $$$$$$$\
//$$  __$$\ $$  __$$\ $$  __$$\ \__|$$  __$$\ $$  _____|\_$$  _|  $$  _____|
//$$ /  $$ |$$ |  \__|$$ /  $$ |$$\ $$$$$$$$ |$$ /        $$ |    \$$$$$$\
//$$ |  $$ |$$ |      $$ |  $$ |$$ |$$   ____|$$ |        $$ |$$\  \____$$\
//$$$$$$$  |$$ |      \$$$$$$  |$$ |\$$$$$$$\ \$$$$$$$\   \$$$$  |$$$$$$$  |
//$$  ____/ \__|       \______/ $$ | \_______| \_______|   \____/ \_______/
//$$ |                    $$\   $$ |
//$$ |                    \$$$$$$  |
//\__|                     \______/
//cc-projects-service

# Create config file
resource "google_secret_manager_secret" "cc_projects_service_qa_config" {
  secret_id = "cc-projects-service-qa"

  labels = {
    env = "qa"
  }

  replication {
    automatic = true
  }
}

# Create config file
resource "google_secret_manager_secret" "cc_projects_service_config" {
  secret_id = "cc-projects-service"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

//                          $$\
//                          $$ |
//      $$\ $$\  $$\  $$\ $$$$$$\
//      \__|$$ | $$ | $$ |\_$$  _|
//      $$\ $$ | $$ | $$ |  $$ |
//      $$ |$$ | $$ | $$ |  $$ |$$\
//      $$ |\$$$$$\$$$$  |  \$$$$  |
//      $$ | \_____\____/    \____/
//$$\   $$ |
//\$$$$$$  |
// \______/

resource "google_secret_manager_secret" "jwt_private_key_qa" {
  secret_id = "jwt-private-key-qa"

  labels = {
    env = "qa"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "jwt_public_key_qa" {
  secret_id = "jwt-public-key-qa"

  labels = {
    env = "qa"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "jwt_private_key" {
  secret_id = "jwt-private-key"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "jwt_public_key" {
  secret_id = "jwt-public-key"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

//                      $$\     $$\
//                      $$ |    $$ |
// $$$$$$\  $$\   $$\ $$$$$$\   $$$$$$$\
// \____$$\ $$ |  $$ |\_$$  _|  $$  __$$\
// $$$$$$$ |$$ |  $$ |  $$ |    $$ |  $$ |
//$$  __$$ |$$ |  $$ |  $$ |$$\ $$ |  $$ |
//\$$$$$$$ |\$$$$$$  |  \$$$$  |$$ |  $$ |
// \_______| \______/    \____/ \__|  \__|

resource "google_secret_manager_secret" "cc_auth_qa" {
  secret_id = "cc-auth-qa"

  labels = {
    env = "qa"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_auth" {
  secret_id = "cc-auth"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

//                    $$\                              $$\
//                    \__|                             $$ |
// $$$$$$\   $$$$$$\  $$\          $$$$$$\   $$$$$$\ $$$$$$\    $$$$$$\  $$\  $$\  $$\  $$$$$$\  $$\   $$\
// \____$$\ $$  __$$\ $$ |$$$$$$\ $$  __$$\  \____$$\\_$$  _|  $$  __$$\ $$ | $$ | $$ | \____$$\ $$ |  $$ |
// $$$$$$$ |$$ /  $$ |$$ |\______|$$ /  $$ | $$$$$$$ | $$ |    $$$$$$$$ |$$ | $$ | $$ | $$$$$$$ |$$ |  $$ |
//$$  __$$ |$$ |  $$ |$$ |        $$ |  $$ |$$  __$$ | $$ |$$\ $$   ____|$$ | $$ | $$ |$$  __$$ |$$ |  $$ |
//\$$$$$$$ |$$$$$$$  |$$ |        \$$$$$$$ |\$$$$$$$ | \$$$$  |\$$$$$$$\ \$$$$$\$$$$  |\$$$$$$$ |\$$$$$$$ |
// \_______|$$  ____/ \__|         \____$$ | \_______|  \____/  \_______| \_____\____/  \_______| \____$$ |
//          $$ |                  $$\   $$ |                                                     $$\   $$ |
//          $$ |                  \$$$$$$  |                                                     \$$$$$$  |
//          \__|                   \______/                                                       \______/

resource "google_secret_manager_secret" "cc_api_gateway_qa" {
  secret_id = "cc-api-gateway-qa"

  labels = {
    env = "qa"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_api_gateway_us" {
  secret_id = "cc-api-gateway-us"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_api_gateway_sg" {
  secret_id = "cc-api-gateway-sg"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_api_gateway_eu" {
  secret_id = "cc-api-gateway-eu"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

//                                            $$\ $$\                                                                                       $$\
//                                            \__|$$ |                                                                                      $$ |
// $$$$$$$\  $$$$$$\  $$$$$$\$$$$\   $$$$$$\  $$\ $$ | $$$$$$\   $$$$$$\         $$$$$$$\  $$$$$$\   $$$$$$\ $$\    $$\ $$$$$$\  $$$$$$$\ $$$$$$\
//$$  _____|$$  __$$\ $$  _$$  _$$\ $$  __$$\ $$ |$$ |$$  __$$\ $$  __$$\       $$  _____|$$  __$$\ $$  __$$\\$$\  $$  |\____$$\ $$  __$$\\_$$  _|
//$$ /      $$ /  $$ |$$ / $$ / $$ |$$ /  $$ |$$ |$$ |$$$$$$$$ |$$ |  \__|      \$$$$$$\  $$$$$$$$ |$$ |  \__|\$$\$$  / $$$$$$$ |$$ |  $$ | $$ |
//$$ |      $$ |  $$ |$$ | $$ | $$ |$$ |  $$ |$$ |$$ |$$   ____|$$ |             \____$$\ $$   ____|$$ |       \$$$  / $$  __$$ |$$ |  $$ | $$ |$$\
//\$$$$$$$\ \$$$$$$  |$$ | $$ | $$ |$$$$$$$  |$$ |$$ |\$$$$$$$\ $$ |            $$$$$$$  |\$$$$$$$\ $$ |        \$  /  \$$$$$$$ |$$ |  $$ | \$$$$  |
// \_______| \______/ \__| \__| \__|$$  ____/ \__|\__| \_______|\__|            \_______/  \_______|\__|         \_/    \_______|\__|  \__|  \____/
//                                  $$ |
//                                  $$ |
//                                  \__|

resource "google_secret_manager_secret" "cc_compiler_servant_qa" {
  secret_id = "cc-compiler-servant-qa"

  labels = {
    env = "qa"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_compiler_servant_us" {
  secret_id = "cc-compiler-servant-us"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_compiler_servant_eu" {
  secret_id = "cc-compiler-servant-eu"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_compiler_servant_sg" {
  secret_id = "cc-compiler-servant-sg"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

//                                            $$\ $$\                                                                 $$\
//                                            \__|$$ |                                                                $$ |
// $$$$$$$\  $$$$$$\  $$$$$$\$$$$\   $$$$$$\  $$\ $$ | $$$$$$\   $$$$$$\          $$$$$$\$$$$\   $$$$$$\   $$$$$$$\ $$$$$$\    $$$$$$\   $$$$$$\
//$$  _____|$$  __$$\ $$  _$$  _$$\ $$  __$$\ $$ |$$ |$$  __$$\ $$  __$$\ $$$$$$\ $$  _$$  _$$\  \____$$\ $$  _____|\_$$  _|  $$  __$$\ $$  __$$\
//$$ /      $$ /  $$ |$$ / $$ / $$ |$$ /  $$ |$$ |$$ |$$$$$$$$ |$$ |  \__|\______|$$ / $$ / $$ | $$$$$$$ |\$$$$$$\    $$ |    $$$$$$$$ |$$ |  \__|
//$$ |      $$ |  $$ |$$ | $$ | $$ |$$ |  $$ |$$ |$$ |$$   ____|$$ |              $$ | $$ | $$ |$$  __$$ | \____$$\   $$ |$$\ $$   ____|$$ |
//\$$$$$$$\ \$$$$$$  |$$ | $$ | $$ |$$$$$$$  |$$ |$$ |\$$$$$$$\ $$ |              $$ | $$ | $$ |\$$$$$$$ |$$$$$$$  |  \$$$$  |\$$$$$$$\ $$ |
// \_______| \______/ \__| \__| \__|$$  ____/ \__|\__| \_______|\__|              \__| \__| \__| \_______|\_______/    \____/  \_______|\__|
//                                  $$ |
//                                  $$ |
//                                  \__|

resource "google_secret_manager_secret" "cc_compiler_master_qa" {
  secret_id = "cc-compiler-master-qa"

  labels = {
    env = "qa"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_compiler_master_us" {
  secret_id = "cc-compiler-master-us"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_compiler_master_eu" {
  secret_id = "cc-compiler-master-eu"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_compiler_master_sg" {
  secret_id = "cc-compiler-master-sg"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

//                $$\ $$\   $$\                                                   $$\
//                $$ |\__|  $$ |                                                  \__|
// $$$$$$\   $$$$$$$ |$$\ $$$$$$\    $$$$$$\   $$$$$$\         $$$$$$\   $$$$$$\  $$\
//$$  __$$\ $$  __$$ |$$ |\_$$  _|  $$  __$$\ $$  __$$\        \____$$\ $$  __$$\ $$ |
//$$$$$$$$ |$$ /  $$ |$$ |  $$ |    $$ /  $$ |$$ |  \__|       $$$$$$$ |$$ /  $$ |$$ |
//$$   ____|$$ |  $$ |$$ |  $$ |$$\ $$ |  $$ |$$ |            $$  __$$ |$$ |  $$ |$$ |
//\$$$$$$$\ \$$$$$$$ |$$ |  \$$$$  |\$$$$$$  |$$ |            \$$$$$$$ |$$$$$$$  |$$ |
// \_______| \_______|\__|   \____/  \______/ \__|             \_______|$$  ____/ \__|
//                                                                      $$ |
//                                                                      $$ |
//                                                                      \__|

resource "google_secret_manager_secret" "cc_editor_api_qa" {
  secret_id = "cc-editor-api-qa"

  labels = {
    env = "qa"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_editor_api_us" {
  secret_id = "cc-editor-api-us"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_editor_api_eu" {
  secret_id = "cc-editor-api-eu"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_editor_api_sg" {
  secret_id = "cc-editor-api-sg"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}
//                    $$\
//                    \__|
// $$$$$$\   $$$$$$\  $$\
// \____$$\ $$  __$$\ $$ |
// $$$$$$$ |$$ /  $$ |$$ |
//$$  __$$ |$$ |  $$ |$$ |
//\$$$$$$$ |$$$$$$$  |$$ |
// \_______|$$  ____/ \__|
//          $$ |
//          $$ |
//          \__|

resource "google_secret_manager_secret" "cc_api_qa" {
  secret_id = "cc-api-qa"

  labels = {
    env = "qa"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_api" {
  secret_id = "cc-api"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

//          $$\
//          $$ |
// $$$$$$$\ $$$$$$$\   $$$$$$\   $$$$$$\   $$$$$$\
//$$  _____|$$  __$$\  \____$$\ $$  __$$\ $$  __$$\
//\$$$$$$\  $$ |  $$ | $$$$$$$ |$$ |  \__|$$$$$$$$ |
// \____$$\ $$ |  $$ |$$  __$$ |$$ |      $$   ____|
//$$$$$$$  |$$ |  $$ |\$$$$$$$ |$$ |      \$$$$$$$\
//\_______/ \__|  \__| \_______|\__|       \_______|

resource "google_secret_manager_secret" "cc_share_qa" {
  secret_id = "cc-share-qa"

  labels = {
    env = "qa"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_share_us" {
  secret_id = "cc-share-us"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_share_eu" {
  secret_id = "cc-share-eu"

  labels = {
    env = "qa"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_share_sg" {
  secret_id = "cc-share-sg"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

//$$\        $$\                   $$\
//$$ |       $$ |                  $$ |
//$$$$$$$\ $$$$$$\   $$$$$$\$$$$\  $$ |         $$$$$$$\  $$$$$$\   $$$$$$\ $$\    $$\  $$$$$$\   $$$$$$\
//$$  __$$\\_$$  _|  $$  _$$  _$$\ $$ |$$$$$$\ $$  _____|$$  __$$\ $$  __$$\\$$\  $$  |$$  __$$\ $$  __$$\
//$$ |  $$ | $$ |    $$ / $$ / $$ |$$ |\______|\$$$$$$\  $$$$$$$$ |$$ |  \__|\$$\$$  / $$$$$$$$ |$$ |  \__|
//$$ |  $$ | $$ |$$\ $$ | $$ | $$ |$$ |         \____$$\ $$   ____|$$ |       \$$$  /  $$   ____|$$ |
//$$ |  $$ | \$$$$  |$$ | $$ | $$ |$$ |        $$$$$$$  |\$$$$$$$\ $$ |        \$  /   \$$$$$$$\ $$ |
//\__|  \__|  \____/ \__| \__| \__|\__|        \_______/  \_______|\__|         \_/     \_______|\__|

resource "google_secret_manager_secret" "cc_html_server_qa" {
  secret_id = "cc-html-server-qa"

  labels = {
    env = "qa"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_html_server_us" {
  secret_id = "cc-html-server-us"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_html_server_eu" {
  secret_id = "cc-html-server-eu"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_html_server_sg" {
  secret_id = "cc-html-server-sg"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}

//$$\   $$\     $$\
//$$ |  $$ |    \__|
//$$ |$$$$$$\   $$\
//$$ |\_$$  _|  $$ |
//$$ |  $$ |    $$ |
//$$ |  $$ |$$\ $$ |
//$$ |  \$$$$  |$$ |
//\__|   \____/ \__|

resource "google_secret_manager_secret" "cc_lti_qa" {
  secret_id = "cc-lti-qa"

  labels = {
    env = "qa"
  }

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "cc_lti" {
  secret_id = "cc-lti"

  labels = {
    env = "production"
  }

  replication {
    automatic = true
  }
}
